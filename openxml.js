/*
   Copyright (c) 2012 Kevin Walton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


var openxml = {};   //namespace

// this is the document file (which is really a zip package)
openxml.DocX = function(){
    var docx = {};

    var zipx = null;
    var template = new openxml.DocxDefaultTemplate();

    // internal filesystem structure
    var structure = template.getStructure();
    function getStructure(){
        return structure;
    }
    function setStructure( struct ){
        structure = struct;
    }


    //TODO: abstract walking the structure to generic methods
    function processDir( path, obj ){
        var files = obj["files"];
        var dirs = obj["dirs"];

        for( var i=0; i< files.length; i++){
            processFile( path+'/'+files[i] );
        }
        for( var dir in dirs ){
            path = path.replace(/^[\/]+/, '');
            //zipx.folder(path+'/'+dir);
            processDir( path+'/'+dir, dirs[dir] );
        }
    }

    function processFile( file ){
        file = file.replace(/^[\/]+/, '');
        //console.log( file );

        var f = getPart( file );
        zipx.file( file, f );
    }


    function compile( callback ){
        // zip and return
        zipx = new JSZip();
        processDir('', structure);
        var docxbin = zipx.generate({compression:"DEFLATE"});
        callback( docxbin );
    }


    function useTemplate( tmpl ){
        template = tmpl;
        structure = tmpl.getStructure();
    }
    function getTemplate(){
        return template;
    }
    function getTemplateOf( filePath ){
        if( template ){
            return template.get( filePath );
        }
        return null;
    }


    var userParts = {};
    function getPart( filePath ){
        if( userParts.hasOwnProperty(filePath) ){
            if( typeof userParts[filePath] == typeof ""){
                return userParts[filePath];
            }else{
                return userParts[filePath].value();
            }
        }else{
            return getTemplateOf( filePath );
        }
    }
    function setPart( filePath, content ){
        userParts[filePath] = content;
    }

    //make public
    docx.setStructure = setStructure;
    docx.getStructure = getStructure;
    docx.useTemplate = useTemplate;
    docx.getTemplate = getTemplate;
    docx.set = setPart;
    docx.get = getPart;
    docx.save = compile;

    return docx;
};  //class DocX


// this is really an 'abstract' class which shouldn't be used... use Docx*Template types instead...
openxml.DocxTemplate = function( templateLoadedCallback ){
    var tmpl = {};
    var loading = true;

    var structure = null;
    function getStructure(){
        return structure;
    }

    var tmplParts = {};
    function getTemplate( filePath ){
        if( tmplParts.hasOwnProperty(filePath) ){
            return tmplParts[filePath];
        }
        return null;
    }

    function isLoaded(){
        return !loading;
    }

    // "protected"
    tmpl._part = function( name, val ){
        tmplParts[name] = val;
    };
    tmpl._structure = function( struct ){
        structure = struct;
    };

    //make public
    tmpl.get = getTemplate;
    tmpl.getStructure = getStructure;
    tmpl.isLoaded = isLoaded;

    // uhhh... this 'abstract' class will never 'load'...

    return tmpl;
};  //class DocxTemplate


//default, minimalistic template
// minimalistic content comes from opening and saving Office Open XML document in LibreOffice
openxml.DocxDefaultTemplate = function( templateLoadedCallback ){
    var tmpl = new openxml.DocxTemplate( templateLoadedCallback );  // "extends"
    //var structure = tmpl._structure;
    //var tmplParts = tmpl._tmplParts;

    // internal filesystem structure
    var docxStructure = {
        "files":["[Content_Types].xml"],
        "dirs":{
            "_rels":{
                "files":[".rels"],
                "dirs":{}
            },
            "docProps":{
                "files":["app.xml","core.xml"],
                "dirs":{}
            },
            "word":{
                "files":["document.xml","fontTable.xml","settings.xml","styles.xml"],
                "dirs":{
                    "_rels":{
                        "files":["document.xml.rels"],
                        "dirs":{}
                    }
                }
            }
        }
    };
    tmpl._structure( docxStructure );

    tmpl._part( '[Content_Types].xml', '<?xml version="1.0" encoding="UTF-8"?>' +
        '<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">' +
            '<Override PartName="/_rels/.rels" ContentType="application/vnd.openxmlformats-package.relationships+xml"/>' +
            '<Override PartName="/word/_rels/document.xml.rels" ContentType="application/vnd.openxmlformats-package.relationships+xml"/>' +
            '<Override PartName="/word/settings.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml"/>' +
            '<Override PartName="/word/document.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml"/>' +
            '<Override PartName="/word/styles.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml"/>' +
            '<Override PartName="/word/fontTable.xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml"/>' +
            '<Override PartName="/docProps/app.xml" ContentType="application/vnd.openxmlformats-officedocument.extended-properties+xml"/>' +
            '<Override PartName="/docProps/core.xml" ContentType="application/vnd.openxmlformats-package.core-properties+xml"/>' +
        '</Types>');

    tmpl._part( '_rels/.rels', '<?xml version="1.0" encoding="UTF-8"?>' +
            '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">' +
                '<Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officedocument/2006/relationships/metadata/core-properties" Target="docProps/core.xml"/>' +
                '<Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties" Target="docProps/app.xml"/>' +
                '<Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="word/document.xml"/>' +
        '</Relationships>');

    tmpl._part( 'docProps/app.xml', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
        '<Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">' +
            '<TotalTime>0</TotalTime>' +
        '</Properties>');

    tmpl._part( 'docProps/core.xml', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
        '<cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcmitype="http://purl.org/dc/dcmitype/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<cp:revision>0</cp:revision>' +
        '</cp:coreProperties>');

    tmpl._part( 'word/_rels/document.xml.rels', '<?xml version="1.0" encoding="UTF-8"?>' +
        '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">' +
            '<Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>' +
            '<Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable" Target="fontTable.xml"/>' +
            '<Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings" Target="settings.xml"/>' +
        '</Relationships>');

    tmpl._part( 'word/fontTable.xml', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
        '<w:fonts xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">' +
            '<w:font w:name="Times New Roman"><w:charset w:characterSet="windows-1252" w:val="00"/><w:family w:val="roman"/><w:pitch w:val="variable"/></w:font>' +
            '<w:font w:name="Symbol"><w:charset w:val="02"/><w:family w:val="roman"/><w:pitch w:val="variable"/></w:font>' +
            '<w:font w:name="Arial"><w:charset w:characterSet="windows-1252" w:val="00"/><w:family w:val="swiss"/><w:pitch w:val="variable"/></w:font>' +
            '<w:font w:name="Times New Roman"><w:charset w:characterSet="utf-8" w:val="80"/><w:family w:val="roman"/><w:pitch w:val="variable"/></w:font>' +
        '</w:fonts>');

    tmpl._part( 'word/settings.xml', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
        '<w:settings xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">' +
            '<w:zoom w:percent="100"/>' +
            '<w:defaultTabStop w:val="709"/>' +
        '</w:settings>');

    tmpl._part( 'word/styles.xml', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
        '<w:styles xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">' +
            '<w:style w:styleId="style0" w:type="paragraph"><w:name w:val="Normal"/><w:next w:val="style0"/><w:pPr><w:widowControl w:val="false"/><w:tabs></w:tabs><w:suppressAutoHyphens w:val="true"/></w:pPr><w:rPr><w:rFonts w:ascii="Times New Roman" w:cs="Lucida Sans" w:eastAsia="SimSun" w:hAnsi="Times New Roman"/><w:color w:val="auto"/><w:sz w:val="24"/><w:szCs w:val="24"/><w:lang w:bidi="hi-IN" w:eastAsia="zh-CN" w:val="en-US"/></w:rPr></w:style>' +
            '<w:style w:styleId="style15" w:type="paragraph"><w:name w:val="Heading"/><w:basedOn w:val="style0"/><w:next w:val="style16"/><w:pPr><w:keepNext/><w:spacing w:after="120" w:before="240"/><w:contextualSpacing w:val="false"/></w:pPr><w:rPr><w:rFonts w:ascii="Arial" w:cs="Lucida Sans" w:eastAsia="SimSun" w:hAnsi="Arial"/><w:sz w:val="28"/><w:szCs w:val="28"/></w:rPr></w:style>' +
            '<w:style w:styleId="style16" w:type="paragraph"><w:name w:val="Text body"/><w:basedOn w:val="style0"/><w:next w:val="style16"/><w:pPr><w:spacing w:after="120" w:before="0"/><w:contextualSpacing w:val="false"/></w:pPr><w:rPr></w:rPr></w:style>' +
            '<w:style w:styleId="style17" w:type="paragraph"><w:name w:val="List"/><w:basedOn w:val="style16"/><w:next w:val="style17"/><w:pPr></w:pPr><w:rPr><w:rFonts w:cs="Lucida Sans"/></w:rPr></w:style>' +
            '<w:style w:styleId="style18" w:type="paragraph"><w:name w:val="Caption"/><w:basedOn w:val="style0"/><w:next w:val="style18"/><w:pPr><w:suppressLineNumbers/><w:spacing w:after="120" w:before="120"/><w:contextualSpacing w:val="false"/></w:pPr><w:rPr><w:rFonts w:cs="Lucida Sans"/><w:i/><w:iCs/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:style>' +
            '<w:style w:styleId="style19" w:type="paragraph"><w:name w:val="Index"/><w:basedOn w:val="style0"/><w:next w:val="style19"/><w:pPr><w:suppressLineNumbers/></w:pPr><w:rPr><w:rFonts w:cs="Lucida Sans"/></w:rPr></w:style>' +
        '</w:styles>');

    tmpl._part( 'word/document.xml', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
        '<w:document xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing">' +
            '<w:body>' +
                '<w:p><w:pPr><w:pStyle w:val="style0"/></w:pPr><w:r><w:rPr></w:rPr><w:t>generated by openxml-js</w:t></w:r></w:p>' +
                '<w:sectPr><w:type w:val="nextPage"/><w:pgSz w:h="15840" w:w="12240"/><w:pgMar w:bottom="1134" w:footer="0" w:gutter="0" w:header="0" w:left="1134" w:right="1134" w:top="1134"/><w:pgNumType w:fmt="decimal"/><w:formProt w:val="false"/><w:textDirection w:val="lrTb"/></w:sectPr>' +
            '</w:body>' +
        '</w:document>');


    //minimalistic content from section 5.5 of the 'Open XML White Paper.pdf'
    /*
     var docxStructure = {
     "files":["[Content_Types].xml","document.xml"],
     "dirs":{
     "_rel":{
     "files":[".rels"],
     "dirs":{}
     }
     }
     };
     tmpl._structure( docxStructure );

     tmpl._part( '[Content_Types].xml', '<?xml version="1.0" encoding="UTF-8"?>' +
     '<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">' +
     '<Default Extension="rels" ContentType="application/vnd.openxmlformats-package.relationships+xml"/>' +
     '<Default Extension="xml" ContentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml"/>' +
     '</Types>');

     tmpl._part( '_rel/.rels', '<?xml version="1.0" encoding="UTF-8"?>' +
     '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">' +
     '<Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="document.xml"/>' +
     '</Relationships>');

     tmpl._part( 'document.xml', '<?xml version="1.0" encoding="UTF-8"?>' +
     '<w:document xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">' +
     '<w:body>' +
     '<w:p><w:r><w:t>Hello, world.</w:t></w:r></w:p>' +
     '</w:body>' +
     '</w:document>');
     */


    // we didn't really load asynchronously, so go ahead and notify as loaded...
    loading = false;
    if( templateLoadedCallback ){
        templateLoadedCallback();
    }

    return tmpl;
};  //class DocxDefaultTemplate


//template that exists on webserver
openxml.DocxRemoteTemplate = function( remotePath, templateLoadedCallback ){
    var tmpl = new openxml.DocxTemplate( templateLoadedCallback );  // "extends"
    //var structure = tmpl._structure;
    //var tmplParts = tmpl._tmplParts;

    //fetch files from server
    var fetcher = openxml.util.AsyncCallGroup( templateLoadedCallback );
    if( fetcher && fetcher.isProcessing() ){
        fetcher.abort();
        fetcher = null;
    }


    //TODO: use generic structure walk
    function processDir( path, obj ){
        var files = obj["files"];
        var dirs = obj["dirs"];

        for( var i=0; i< files.length; i++){
            processFile( path+'/'+files[i] );
        }
        for( var dir in dirs ){
            path = path.replace(/^[\/]+/, '');
            processDir( path+'/'+dir, dirs[dir] );
        }
    }

    function processFile( file ){
        file = file.replace(/^[\/]+/, '');
        //console.log( file );
        fetcher.get( remotePath+'/'+file, function(data){
            //console.log( "got file: "+remotePath+'/'+file );
            tmpl._part( file, data );
        } );
    }

    //fetch template structure json from path
    var structure = tmpl._structure;
    $.ajax({
        url: remotePath+'/openxml-package.json.js',
        dataType:'json',
        success: function( data, textStatus, jqXHR ){
            structure = data;
            tmpl._structure(structure);
            console.log( 'template package structure loaded' );
            processDir( '', structure );
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log( 'template package structure not found, using default' );
            processDir( '', structure );
        }
    });

    return tmpl;
};  //class DocxRemoteTemplate


//template that exists in local filesystem
openxml.DocxLocalTemplate = function( templateLoadedCallback ){
    var tmpl = new openxml.DocxTemplate( templateLoadedCallback );  // "extends"
    var structure = tmpl._structure;
    var tmplParts = tmpl._tmplParts;

    //TODO: possibly using a local file reader?  Not really a point of focus right now

    return tmpl;
};  //class DocxLocalTemplate



// this is the heart of the document
openxml.WordProcessingML = function(){
    var doc = {};

    var ml = "";

    //TODO: use a real DOM rather than simple text concatenation


    function getValue(){
        var val = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
            '<w:document xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" ' +
                'xmlns:mo="http://schemas.microsoft.com/office/mac/office/2008/main" ' +
                'xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" ' +
                'xmlns:mv="urn:schemas-microsoft-com:mac:vml" xmlns:o="urn:schemas-microsoft-com:office:office" ' +
                'xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" ' +
                'xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" ' +
                'xmlns:v="urn:schemas-microsoft-com:vml" ' +
                'xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" ' +
                'xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" ' +
                'xmlns:w10="urn:schemas-microsoft-com:office:word" ' +
                'xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" ' +
                'xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" ' +
                'xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" ' +
                'xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" ' +
                'xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" ' +
                'xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" ' +
                'mc:Ignorable="w14 wp14">' +
                '<w:body>';
        /*
        var val = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
            '<w:document xmlns:o="urn:schemas-microsoft-com:office:office"' +
                'xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"' +
                'xmlns:v="urn:schemas-microsoft-com:vml"' +
                'xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"' +
                'xmlns:w10="urn:schemas-microsoft-com:office:word"' +
                'xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing">' +
                '<w:body>';
        */
        val += ml;

        val +=  '<w:p />' +
                '<w:sectPr>' +
                    '<w:pgSz w:w="12240" w:h="15840"/>' +
                    '<w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="720" w:footer="720" w:gutter="0"/>' +
                    '<w:cols w:space="720"/>' +
                '</w:sectPr>' +
                '</w:body>' +
            '</w:document>';
        /*
        val += '<w:sectPr>' +
                    '<w:type w:val="nextPage"/>' +
                    '<w:pgSz w:h="15840" w:w="12240"/>' +
                    '<w:pgMar w:bottom="1134" w:footer="0" w:gutter="0" w:header="0" w:left="1134" w:right="1134" w:top="1134"/>' +
                    '<w:pgNumType w:fmt="decimal"/>' +
                    '<w:formProt w:val="false"/>' +
                    '<w:textDirection w:val="lrTb"/>' +
                '</w:sectPr>' +
                '</w:body>' +
            '</w:document>';
        */
        return val;
    }

    function append( elem ){
        ml += '\n'+elem;
    }

    function appendFromHTML( text, tag, props ){
        var m = "";
        switch( tag ){
            case 'p':
                m = '<w:p>' +
                        '<w:r>' +
                            '<w:t>'+ trim(text) +'</w:t>' +
                        '</w:r>' +
                    '</w:p>';
                break;
            case 'h1':
            case 'h2':
            case 'h3':
            case 'h4':
            case 'h5':
                var styles = {
                    'h1':"Heading1",
                    'h2':"Heading2",
                    'h3':"Heading3",
                    'h4':"Heading4",
                    'h5':"Heading5"
                };
                m = '<w:p/><w:p>' +
                        '<w:pPr><w:pStyle w:val="'+ styles[tag] +'"/></w:pPr>' +
                        '<w:r>' +
                            '<w:t>'+ trim(text) +'</w:t>' +
                        '</w:r>' +
                    '</w:p>';
                break;
            case 'table':
                m = '<w:tbl>' +
                        '<w:tblPr>' +
                            '<w:tblStyle w:val="TableGrid"/>' +
                            '<w:tblW w:w="0" w:type="auto"/>' +
                            '<w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>' +
                        '</w:tblPr>';
                break;
            case '/table':
                m = '</w:tbl>';
                break;
            case 'tr':
                if( typeof text != typeof [] ){
                    text = [text];
                }

                m = '<w:tr>';
                for( var t=0; t<text.length; t++){
                    m += '\n<w:tc><w:p>';
                    if( props && props.hasOwnProperty('bold') ){
                        m += '<w:pPr><w:rPr><w:b/></w:rPr></w:pPr>' +
                                '<w:r><w:rPr><w:b/></w:rPr><w:t>'+ trim(text[t]) +'</w:t></w:r>';
                    }else{
                        m += '<w:r><w:t>'+ trim(text[t]) +'</w:t></w:r>';
                    }
                    m += '</w:p></w:tc>';
                }
                m += '\n</w:tr>';
                break;
            default:
                m = trim(text);
        }

        append( m );
    }

    // trim11 from http://blog.stevenlevithan.com/archives/faster-trim-javascript
    function trim (str) {
        str = str.replace(/^\s+/, '');
        for (var i = str.length - 1; i >= 0; i--) {
            if (/\S/.test(str.charAt(i))) {
                str = str.substring(0, i + 1);
                break;
            }
        }
        return str;
    }


    function xmlescape(text){
        text = text.replace(/\&/g, "&amp;");
        text = text.replace(/</g,  "&lt;");
        text = text.replace(/>/g,  "&gt;");
        text = text.replace(/'/g,  "&apos;");
        text = text.replace(/"/g,  "&quot;");
        return text;
    }


    function appendFromDOM( elem ){

        elem = $(elem);
        var elements = elem.find("p, h1, h2, h3, h4, code, table");
        elements.each(function() {
            var tag = this.nodeName.toLowerCase();

            if( tag == "table" ){
                var tbl = $(this);
                appendFromHTML( '', 'table' );

                var hdr = tbl.find("thead tr");
                hdr.each( function(){
                    appendTableRow( this );
                });

                var rows = tbl.find("tbody tr");
                rows.each( function(){
                    appendTableRow( this );
                });
                rows = tbl.children("tr");
                rows.each( function(){
                    appendTableRow( this );
                });

                appendFromHTML( '', '/table' );

            }else if( tag == "code" ){
                appendFromHTML( xmlescape( $(this).text() ), 'p' );
            }else{
                appendFromHTML( xmlescape( $(this).text() ), tag );
            }

        });


        function appendTableRow( row ){
            var col;
            var rowVals = [];
            var isRequired = false;
            $(row).children('th,td').each( function(){
                col = $(this);
                if( col.hasClass('required') ){
                    isRequired = true;
                }
                rowVals.push( xmlescape( $(this).text() ) );
            });

            if( isRequired ){
                appendFromHTML( rowVals, 'tr', {bold:true} );
            }else{
                appendFromHTML( rowVals, 'tr' );
            }
        }
    }



    //make public
    doc.append = append;
    doc.value = getValue;
    doc.appendFromHTML = appendFromHTML;
    doc.appendFromDOM = appendFromDOM;

    return doc;
};  //class WordProcessingML



// utility class for grouping asynchronous ajax calls
// registers a callback to call when the entire group is finished
openxml.util = {};
openxml.util.AsyncCallGroup = function( callback ){
    var obj = {};

    var total = 0;
    var completed = 0;

    var processing = false;
    function isProcessing(){
        return processing;
    }

    function getUrl( url, onResult, onFail ){
        processing = true;
        total += 1;
        /*
        (function(u){
            setTimeout( function(){ onResult(url) }, 100 );
        })(url);
        */
        //TODO: switch on... just a test right now

        (function( u ){
            $.ajax({
                url:u,
                dataType: 'text',
                success: onSuccess,
                error: onError
            });

            function onSuccess(data){
                if( onResult ){
                    onResult(data);
                }
                completed += 1;
                obj.checkComplete();
             }
             function onError(data){
                if( onFail ){
                    onFail(data);
                }
                obj.fail( u );
             }
         })(url);

    }

    function checkComplete(){
        if( completed == total ){
            processing = false;
            var retObj = {"type":"SUCCESS"};
            callback(retObj);
        }
    }

    function fail( msg ){
        var failObj = {"type":"ERROR","message":msg};
        callback(failObj);
    }

    function abort(){
        if( processing ){
            var retObj = {"type":"ABORTED"};
            callback(retObj);
        }
    }

    //make public
    obj.get = getUrl;
    obj.checkComplete = checkComplete;
    obj.fail = fail;
    obj.isProcessing = isProcessing;
    obj.abort = abort;

    return obj;
};  //class AsyncCallGroup


// constants for mime types
openxml.mime = {};
openxml.mime.DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
