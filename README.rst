==========
OpenXML-JS
==========

OpenXML-JS is a javascript library for creating Office Open XML documents (such as docx) within the browser.  The primary use case at this point in time is to enable developers to offer the saving of content displayed within a webapp as a document which can then be edited and formatted by end-users.

Office Open XML documents are currently supported by Microsoft Office, LibreOffice, Google Docs, and a host of other modern office applications (http://en.wikipedia.org/wiki/Office_Open_XML_software).

For information about the Office Open XML file formats, see ECMA-376 (http://www.ecma-international.org/publications/standards/Ecma-376.htm).


The OpenXML-JS library depends on the following third-party javascript libraries:

JSZip (http://stuartk.com/jszip/) used under the MIT license.

JQuery (http://jquery.com/) used under the MIT license.

**License**

Copyright (c) 2012 Kevin Walton

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
